#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\begin_preamble
\usepackage{dsfont}
\end_preamble
\use_default_options true
\maintain_unincluded_children false
\language american
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref true
\pdf_bookmarks true
\pdf_bookmarksnumbered false
\pdf_bookmarksopen false
\pdf_bookmarksopenlevel 1
\pdf_breaklinks false
\pdf_pdfborder false
\pdf_colorlinks false
\pdf_backref false
\pdf_pdfusetitle false
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date true
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 1in
\topmargin 1in
\rightmargin 1in
\bottommargin 1in
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style plain
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title

\lang english
Hydrodynamics
\end_layout

\begin_layout Author

\lang english
Omry Cohen
\end_layout

\begin_layout Standard

\lang english
\begin_inset CommandInset toc
LatexCommand tableofcontents

\end_inset


\end_layout

\begin_layout Standard

\lang english
\begin_inset Newpage pagebreak
\end_inset


\end_layout

\begin_layout Part
Hydrodynamics Equations for Non-Viscous Fluids
\end_layout

\begin_layout Standard
Hydrodynamic flow at position 
\begin_inset Formula $\vec{r}$
\end_inset

 and time 
\begin_inset Formula $t$
\end_inset

 is characterized by 6 variables:
\end_layout

\begin_layout Itemize
Velocity field 
\begin_inset Formula $\vec{u}$
\end_inset

 (3 variables)
\end_layout

\begin_layout Itemize
Density 
\begin_inset Formula $\rho$
\end_inset

 (or specific volume 
\begin_inset Formula $v=1/\rho$
\end_inset

)
\end_layout

\begin_layout Itemize
Pressure 
\begin_inset Formula $p$
\end_inset


\end_layout

\begin_layout Itemize
Internal energy per unit mass 
\begin_inset Formula $e$
\end_inset


\end_layout

\begin_layout Standard
Therefore, 6 equations are needed in order to describe the fluid's dynamics.
 
\end_layout

\begin_layout Standard
As a reminder, the material (Lagrangian) derivative obeys 
\begin_inset Formula 
\[
\frac{Df}{Dt}=\frac{\partial f}{\partial t}+\left(\vec{u}\cdot\vec{\nabla}\right)f
\]

\end_inset

 for 
\begin_inset Formula $f$
\end_inset

 being either a scalar or vector field.
\end_layout

\begin_layout Section
General Geometry
\end_layout

\begin_layout Subsection
Conservation of Mass 
\end_layout

\begin_layout Standard
The conservation of mass can be derived most easily in the Eulerian framework.
\end_layout

\begin_layout Standard
The change in the density 
\begin_inset Formula $\rho$
\end_inset

 at some point 
\begin_inset Formula $\vec{r}$
\end_inset

 is caused by mass flowing in or out the volume element 
\begin_inset Formula $dV$
\end_inset

 at 
\begin_inset Formula $\vec{r}$
\end_inset

.
 The flux of this flow is 
\begin_inset Formula $\rho\vec{u}$
\end_inset

, and therefore the mass conservation condition is manifested in a continuity
 equation for the density:
\begin_inset Formula 
\begin{equation}
\frac{\partial\rho}{\partial t}+\vec{\nabla}\left(\rho\vec{u}\right)=0\label{eq:MassConservationEuler}
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
For the Lagrangian description, equation 
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:MassConservationEuler"
plural "false"
caps "false"
noprefix "false"

\end_inset

 becomes
\begin_inset Formula 
\begin{equation}
\frac{D\rho}{Dt}+\rho\vec{\nabla}\vec{u}=0
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
or using the specific volume
\begin_inset Formula 
\begin{equation}
\frac{Dv}{Dt}-v\vec{\nabla}\vec{u}=0\label{eq:MassConservationLagrange}
\end{equation}

\end_inset


\end_layout

\begin_layout Subsection
Conservation of Momentum
\end_layout

\begin_layout Standard
The conservation of momentum can be derived most easily in the Lagrangian
 framework.
\end_layout

\begin_layout Standard
Neglecting the effects of viscosity, the only forces that acts upon a mass
 element 
\begin_inset Formula $dm$
\end_inset

 is due to (gradients of) the pressure.
 The velocity 
\begin_inset Formula $\vec{u}$
\end_inset

 can be thought of as the momentum per unit mass 
\begin_inset Formula $\vec{u}=\frac{\vec{u}dm}{dm}$
\end_inset

.
 The change of this quantity with time is (by definition) the force per
 unit mass.
 The force per unit volume is 
\begin_inset Formula $-\vec{\nabla}p$
\end_inset

, so the force per unit mass is 
\begin_inset Formula $-v\vec{\nabla}p$
\end_inset

.
 Overall, the momentum conservation equation is 
\begin_inset Formula 
\begin{equation}
\frac{D\vec{u}}{Dt}=-v\vec{\nabla}p\label{eq:MomentumConservationLagrange}
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
Note that this vector equation is composed of 3 scalar equations.
 For the Eulerian description, equation 
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:MomentumConservationLagrange"
plural "false"
caps "false"
noprefix "false"

\end_inset

 becomes
\begin_inset Formula 
\begin{equation}
\frac{\partial\vec{u}}{\partial t}+\left(\vec{u}\cdot\vec{\nabla}\right)\vec{u}=-v\vec{\nabla}p\label{eq:eq:MomentumConservationEuler}
\end{equation}

\end_inset


\end_layout

\begin_layout Subsection
Conservation of Energy
\end_layout

\begin_layout Standard
The conservation of energy can be derived most easily in the Lagrangian
 framework.
\end_layout

\begin_layout Standard
Using the first law of thermodynamics, 
\begin_inset Formula $\mathrm{d}e=\delta Q-p\mathrm{d}v$
\end_inset

 where 
\begin_inset Formula $Q$
\end_inset

 is the heat transfer per unit mass.
 Therefore we get 
\begin_inset Formula 
\begin{equation}
\frac{De}{Dt}+p\frac{Dv}{Dt}=\dot{Q}\label{eq:EnergyConservationLagrange}
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
For the Eulerian description, substituting 
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:MassConservationEuler"
plural "false"
caps "false"
noprefix "false"

\end_inset

 in 
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:EnergyConservationLagrange"
plural "false"
caps "false"
noprefix "false"

\end_inset

 we get
\begin_inset Formula 
\begin{equation}
\frac{\partial}{\partial t}\left(e\rho+\frac{1}{2}\rho u^{2}\right)+\vec{\nabla}\left[\rho\vec{u}\left(\frac{1}{2}u^{2}+e+\frac{p}{\rho}\right)\right]=\rho\dot{Q}\label{eq:eq:EnergyConservationEuler}
\end{equation}

\end_inset


\end_layout

\begin_layout Subsection
The Equation of State
\end_layout

\begin_layout Standard
We have 6 variables, but the conservation laws gave only 5 equations.
 The sixth equation is known as the equation of state (EoS).
 It depends on the material and holds the functional relation between its
 thermodynamic variables:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
f\left(p,v,e\right)=0\label{eq:EOS}
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
For ideal gas:
\begin_inset Formula 
\[
pv=\left(\gamma-1\right)e
\]

\end_inset


\end_layout

\begin_layout Section
Various 1D Geometries
\end_layout

\begin_layout Standard
In this section we shall derive the equations for different 1 dimensional
 geometries in the Lagrangian framework.
 
\end_layout

\begin_layout Standard
Let us denote the spatial (Eulerian) coordinate by 
\begin_inset Formula $r$
\end_inset

.
 We define the Lagrangian coordinate 
\begin_inset Formula $m$
\end_inset

 as the mass from the origin to 
\begin_inset Formula $r$
\end_inset

 at 
\begin_inset Formula $t=0$
\end_inset

.
\end_layout

\begin_layout Subsection
Planar
\end_layout

\begin_layout Standard
For planar geometry
\begin_inset Formula 
\begin{equation}
m=\intop\frac{dr}{v}\to\frac{\partial r}{\partial m}=v
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
Equations 
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:MassConservationLagrange"
plural "false"
caps "false"
noprefix "false"

\end_inset

, 
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:MomentumConservationLagrange"
plural "false"
caps "false"
noprefix "false"

\end_inset

 and 
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:EnergyConservationLagrange"
plural "false"
caps "false"
noprefix "false"

\end_inset

 become
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{gather}
\frac{Dv}{Dt}-\frac{\partial u}{\partial m}=0\\
\frac{Du}{Dt}+\frac{\partial p}{\partial m}=0\\
\frac{De}{Dt}+p\frac{Dv}{Dt}=\dot{Q}
\end{gather}

\end_inset


\end_layout

\begin_layout Subsection
Spherical
\end_layout

\begin_layout Standard
For spherical geometry
\begin_inset Formula 
\begin{equation}
m=\intop4\pi r^{2}\frac{1}{v}dr\to\frac{\partial r}{\partial m}=\frac{v}{4\pi r^{2}}
\end{equation}

\end_inset

so
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{gather}
\frac{Dv}{Dt}-4\pi r^{2}\frac{\partial u}{\partial m}-2v\frac{u}{r}=0\\
\frac{Du}{Dt}+4\pi r^{2}\frac{\partial p}{\partial m}=0\\
\frac{De}{Dt}+p\frac{Dv}{Dt}=\dot{Q}
\end{gather}

\end_inset


\end_layout

\begin_layout Subsection
Cylindrical 
\end_layout

\begin_layout Standard
For cylindrical geometry
\begin_inset Formula 
\begin{equation}
m=\intop2\pi r\frac{1}{v}dr\to\frac{\partial r}{\partial m}=\frac{v}{2\pi r}
\end{equation}

\end_inset

so
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{gather}
\frac{Dv}{Dt}-2\pi r\frac{\partial u}{\partial m}-v\frac{u}{r}=0\\
\frac{Du}{Dt}+2\pi r\frac{\partial p}{\partial m}=0\\
\frac{De}{Dt}+p\frac{Dv}{Dt}=\dot{Q}
\end{gather}

\end_inset


\end_layout

\begin_layout Subsection
General
\end_layout

\begin_layout Standard
Generally, defining 
\begin_inset Formula $\left(\alpha,\beta\right)=\left(0,1\right),\left(2,4\pi\right),\left(1,2\pi\right)$
\end_inset

 for planar, spherical and cylindrical geometries respectively we get
\begin_inset Formula 
\begin{equation}
\frac{\partial r}{\partial m}=\frac{v}{\beta r^{\alpha}}
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
where 
\begin_inset Formula $m$
\end_inset

 is the accumulating mass per unit 
\begin_inset Formula $\text{length}^{2-\alpha}$
\end_inset

, and
\begin_inset Formula 
\begin{gather}
\frac{Dv}{Dt}-\beta r^{\alpha}\frac{\partial u}{\partial m}-\frac{\alpha vu}{r}=0\label{eq:MassConservation1dGeneral}\\
\frac{Du}{Dt}+\beta r^{\alpha}\frac{\partial p}{\partial m}=0\label{eq:MomentumConservation1dGeneral}\\
\frac{De}{Dt}+p\frac{Dv}{Dt}=\dot{Q}\label{eq:EnergyConservation1dGeneral}
\end{gather}

\end_inset


\end_layout

\begin_layout Part
Developing the 1D Numeric Scheme 
\end_layout

\begin_layout Section
Artificial Viscosity 
\end_layout

\begin_layout Standard
Assuming 
\begin_inset Formula $\dot{q}=0$
\end_inset

, our equations of motion seem to be adiabatic (each fluid parcel changes
 its energy only due to adiabatic compression/expansion, while keeping its
 entropy constant).
 This fact prevents our equations from describing shock waves (propagating
 discontinuities), as they necessarily increase the entropy (see Zel'dovich
 chapter 2).
 This issue is caused by the fact that we used local (differential) conservation
 laws instead of their global (integral) form, which is more physical.
 
\end_layout

\begin_layout Standard
A solution for this issue was suggested by Von-Neumann and Richtmyer in
 1950 in a paper called 
\begin_inset Quotes qld
\end_inset

A Method for the Numerical Calculation of Hydrodynamic Shocks
\begin_inset Quotes qrd
\end_inset

.
 The idea was to add a term called 
\begin_inset Quotes qld
\end_inset

artificial viscosity
\begin_inset Quotes qrd
\end_inset

 to the pressure, such that it will become significant only when the cell
 exhibits major steepening.
 A possible such term is 
\begin_inset Formula 
\[
q_{i+\frac{1}{2}}^{n}=\begin{cases}
\frac{\sigma}{v_{i+\frac{1}{2}}^{n}}\left(u_{i+1}^{n-\frac{1}{2}}-u_{i}^{n-\frac{1}{2}}\right)^{2} & u_{i}^{n-\frac{1}{2}}>u_{i+1}^{n-\frac{1}{2}}\\
0 & u_{i}^{n-\frac{1}{2}}\leq u_{i+1}^{n-\frac{1}{2}}
\end{cases}
\]

\end_inset

for some dimensionless 
\begin_inset Formula $\sigma\approx1$
\end_inset

.
 This term, when added to the numeric scheme, allows us to artificially
 
\begin_inset Quotes qld
\end_inset

inject
\begin_inset Quotes qrd
\end_inset

 entropy to the system near steepenings, and to successfully describe shock
 waves.
\end_layout

\begin_layout Section
Numeric Scheme 
\end_layout

\begin_layout Standard
For the discretization, we shall denote by 
\begin_inset Formula $f_{i}^{n}$
\end_inset

 the value of 
\begin_inset Formula $f$
\end_inset

 at time-step 
\begin_inset Formula $n$
\end_inset

 and (Lagrangian) position 
\begin_inset Formula $i$
\end_inset

.
 Integer position indices stand for partition positions, and half-integer
 indices stand for the cells centers.
\end_layout

\begin_layout Standard
The mass per unit 
\begin_inset Formula $\text{length}^{2-\alpha}$
\end_inset

 of the cell between the 
\begin_inset Formula $i$
\end_inset

 and the 
\begin_inset Formula $i+1$
\end_inset

 partitions is 
\begin_inset Formula $m_{i+1/2}$
\end_inset

, and the specific volume is
\begin_inset Formula 
\begin{equation}
v_{i+1/2}^{n}=\frac{\frac{\beta}{\alpha+1}\left[\left(r_{i+1}^{n}\right)^{\alpha+1}-\left(r_{i}^{n}\right)^{\alpha+1}\right]}{m_{i+1/2}}
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
Equation 
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:MassConservation1dGeneral"
plural "false"
caps "false"
noprefix "false"

\end_inset

 holds trivially, since the mass of each mass element is constant.
 Equation 
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:MomentumConservation1dGeneral"
plural "false"
caps "false"
noprefix "false"

\end_inset

 can be interpreted as a Newtonian forces equation on the partitions:
\begin_inset Formula 
\begin{equation}
a_{i}^{n}=-\beta\left(r_{i}^{n}\right)^{\alpha}\frac{p_{i+1/2}^{n}+q_{i+1/2}^{n}-p_{i-1/2}^{n}-q_{i-1/2}^{n}}{m_{i}}\label{eq:partition_acceleration}
\end{equation}

\end_inset

where 
\begin_inset Formula $a_{i}^{n}=\frac{Du_{i}^{n}}{Dt}$
\end_inset

 is the partition's acceleration and
\begin_inset Formula 
\begin{equation}
m_{i}=\frac{m_{i+1/2}+m_{i-1/2}}{2}
\end{equation}

\end_inset

is the partition's mass.
 This acceleration is assumed to be calculated in the previous timestep.
 
\end_layout

\begin_layout Standard
The timestep calculation staarts by calculating the new velocities and positions
 of the partitions using the leapfrog scheme to yield
\begin_inset Formula 
\begin{gather}
u_{i}^{n+1/2}=u_{i}^{n}+a_{i}^{n}\frac{\Delta t}{2}\\
r_{i}^{n+1}=r_{i}^{n}+u_{i}^{n+1/2}\Delta t
\end{gather}

\end_inset

 and we can calculate the 
\begin_inset Formula $u^{n+1/2}$
\end_inset

s and the 
\begin_inset Formula $r^{n+1}$
\end_inset

s, and than the 
\begin_inset Formula $q^{n+1}$
\end_inset

s and 
\begin_inset Formula $v^{n+1}$
\end_inset

s.
 
\end_layout

\begin_layout Standard
Discretizing equation 
\begin_inset CommandInset ref
LatexCommand ref
reference "eq:EnergyConservation1dGeneral"
plural "false"
caps "false"
noprefix "false"

\end_inset

 we get
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
e_{i+1/2}^{n+1}-e_{i+1/2}^{n}+\frac{p_{i+1/2}^{n+1}+q_{i+1/2}^{n+1}+p_{i+1/2}^{n}+q_{i+1/2}^{n}}{2}\left(v_{i+1/2}^{n+1}-v_{i+1/2}^{n}\right)=\Delta Q
\end{equation}

\end_inset


\end_layout

\begin_layout Standard
For ideal gas:
\end_layout

\begin_layout Standard
\begin_inset Formula 
\begin{equation}
\frac{p_{i+1/2}^{n+1}v_{i+1/2}^{n+1}}{\gamma-1}=e_{i+1/2}^{n+1}
\end{equation}

\end_inset

so overall
\begin_inset Formula 
\begin{gather}
\frac{p_{i+1/2}^{n+1}v_{i+1/2}^{n+1}-p_{i+1/2}^{n}v_{i+1/2}^{n}}{\gamma-1}+\frac{p_{i+1/2}^{n+1}+q_{i+1/2}^{n+1}+p_{i+1/2}^{n}+q_{i+1/2}^{n}}{2}\left(v_{i+1/2}^{n+1}-v_{i+1/2}^{n}\right)=\Delta Q
\end{gather}

\end_inset

and we can calculate the 
\begin_inset Formula $p^{n+1}$
\end_inset

s:
\begin_inset Formula 
\begin{equation}
p_{i+1/2}^{n+1}=\frac{\Delta Qm_{i+1/2}+\frac{v_{i+1/2}^{n}m_{i+1/2}}{\gamma-1}p_{i+1/2}^{n}-\frac{v_{i+1/2}^{n+1}m_{i+1/2}-v_{i+1/2}^{n}m_{i+1/2}}{2}\left(q_{i+1/2}^{n+1}+p_{i+1/2}^{n}+q_{i+1/2}^{n}\right)}{\frac{v_{i+1/2}^{n+1}m_{i+1/2}}{\gamma-1}+\frac{v_{i+1/2}^{n+1}m_{i+1/2}-v_{i+1/2}^{n}m_{i+1/2}}{2}}
\end{equation}

\end_inset

where we used 
\begin_inset Formula $v_{i+1/2}m_{i+1/2}$
\end_inset

 instead of 
\begin_inset Formula $v_{i+1/2}$
\end_inset

 since 
\begin_inset Formula 
\begin{equation}
v_{i+1/2}^{n}m_{i+1/2}=\frac{\beta}{\alpha+1}\left[\left(r_{i+1}^{n}\right)^{\alpha+1}-\left(r_{i}^{n}\right)^{\alpha+1}\right]
\end{equation}

\end_inset

which is nicer.
 We can now advance the velocity another half-step by calculating 
\begin_inset Formula $a_{i}^{n+1}$
\end_inset

 and
\begin_inset Formula 
\[
u_{i}^{n+1}=u_{i}^{n+1/2}+a_{i}^{n+1}\frac{\Delta t}{2}
\]

\end_inset


\end_layout

\end_body
\end_document
